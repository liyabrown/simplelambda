using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.SystemTextJson.DefaultLambdaJsonSerializer))]

namespace SimpleLambda
{
    public class Function
    {/// <summary>
     /// A simple function that takes a string and does a ToUpper
     /// </summary>
     /// <param name="input"></param>
     /// <param name="context"></param>
     /// <returns></returns>
        public bool FunctionHandler(ILambdaContext context)
        {
            var isSuccess = false;
            context.Logger.Log("Begin function");

            var InvoiceIdList = new List<string>();


            try
            {
                using (var Conn = new SqlConnection(System.Environment.GetEnvironmentVariable("championProductsDBConnectionString")))
                {
                    using (var Cmd = new SqlCommand($"SELECT invoiceId from invoice", Conn))
                    {
                        Conn.Open();

                        SqlDataReader rdr = Cmd.ExecuteReader();

                        //loop through the results
                        while (rdr.Read())
                        {
                            InvoiceIdList.Add(rdr[0].ToString());
                        }
                    }
                }


                var Conn2 = new SqlConnection(System.Environment.GetEnvironmentVariable("championProductsDBConnectionString"));

                foreach (var item in InvoiceIdList)
                {
                    using (var updateCmd = new SqlCommand($"UPDATE dbo.Invoice SET PaidDate = GETDATE() WHERE invoiceId = " + item, Conn2))
                    {
                        Conn2.Open();
                        updateCmd.ExecuteNonQuery();
                        Conn2.Close();
                    }

                }

                isSuccess = true;
            }
            catch (Exception)
            {

                isSuccess = false;
            }


            return isSuccess;
        }
    }
}